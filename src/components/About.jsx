import React from "react";

const About = () => {
  return (
    <div
      name="about"
      className="w-full h-screen bg-gradient-to-b from-gray-800 to-black text-white"
    >
      <div className="max-w-screen-lg p-4 mx-auto flex flex-col justify-center w-full h-full">
        <div className="pb-8">
          <p className="text-4xl font-bold inline border-b-4 border-gray-500">
            About
          </p>
        </div>

        <p className="text-xl mt-20">
        Passionate about transforming ideas into products that 
        people enjoy interacting with. As a Front End Developer, 
        I have gained strong technical skills that enabled me to engage with 
         a remarkable range of clients. 
         I am eager to be challenged in order to grow and further expand my IT skills to deliver more efficient outcomes.
        </p>

        <br />

        <p className="text-xl">
        I developed a sense of flexibility in 
        creating projects with different sizes and 
        architectures while providing a clean code finish . 
        I also gained a significant experience in creating user flows and optimal scenarios with Figma
         providing the best user experience by integrating the Figma designs
          into identical UI that suit clients' needs.
        </p>
      </div>
    </div>
  );
};

export default About;
